<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
class Search extends MY_Controller {
    private $page_line_max = 14;
    private $template;
    function __construct() {
        parent::__construct();
        $this->load->library('twig');
        $this->viewData['base_url'] = base_url();
        $this->viewData['total_users'] = $this->Musers->get_user_total_number();
        if (UserControl::LoggedIn()) {
            $this->viewData['user_login_flag'] = true;
        } else {
            $this->viewData['user_login_flag'] = false;
        }
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $temp = 'https://';
        } else {
            $temp = 'http://';
        }
        $string = $_SERVER["HTTP_HOST"];
        $dome = preg_replace('/onayami/i', 'www', $string);
        $this->viewData['wwwjoyspe'] = $temp . $dome . "/";
    }

    public function index()
    {
        if ($this->input->get('input_keyword')) {
            $this->load->model("Mbbs");
            $keyword = $this->input->get('input_keyword');
            $this->viewData['keyword'] = $this->input->get('input_keyword');
            $list_all_num = $this->Mbbs->search_keyword_consultation_list_num($keyword);
            // pagination
            $this->set_pagination('search_keyword', $list_all_num);

            $offset = $faq_page = null;
            if ($this->input->get('faq_page')) {
                $faq_page = $this->input->get('faq_page');
                $offset =  ($faq_page - 1) * $this->page_line_max;
            }

            $consultation_list_ar = $this->Mbbs->search_keyword_consultation_list($keyword, $this->page_line_max, $offset);
            $this->viewData['consultation_list_ar'] = $consultation_list_ar;
        }
        $consultation_list_all_ar = $this->Mbbs->get_consultation_list_all($this->page_line_max, array(), null, 'sidebar');
        $this->viewData['consultation_list_all_ar'] = $consultation_list_all_ar;

        $this->template = 'search/search_results.html';
        $this->twig->display($this->template, $this->viewData);
    }

    public function ajaxSearchKeywordConsultation()
    {
        $post = $this->input->post();
        if(empty($post['keyword'])) return;
        $this->load->model("Mbbs");
        $consultation_list_ar = $this->Mbbs->search_keyword_consultation_list($post['keyword']);
        if($consultation_list_ar) {
            var_dump($consultation_list_ar);
        }
    }

    public function consultation_list($big_category_id = null, $category_id = null)
    {
        $this->load->model("Mbbs");
        $sidebar_param = array();
        if ($big_category_id == null && $category_id == null) {
            echo "無し";
            $keyword = $this->input->get('keyword');

            $consultation_list_ar = $this->Mbbs->search_keyword_consultation_list($keyword);
            var_dump($consultation_list_ar);

            echo $keyword;
        } elseif ($big_category_id != null && $category_id == null) {
            $big_category_info_ar = $this->Madmin->get_big_category($big_category_id);
            if (empty($big_category_info_ar)) {
                $this->template = '404.html';
            } else {
                $this->viewData['big_category_info'] = $big_category_info_ar[0];
                $big_category_name = $big_category_info_ar[0]['name'];
                $category_info_ar = $this->Madmin->get_categorys($big_category_id);
                $this->viewData['category_info_ar'] = $category_info_ar;

                $list_all_num = $this->Mbbs->search_consultation_list_num($big_category_id);
                // pagination
                $this->set_pagination('category', $list_all_num, $big_category_id);

                $sidebar_param = array(
                    'row' => 'bt.big_cate_id',
                    'value' => $big_category_id 
                );
        
                $offset = $faq_page = null;
                if ($this->input->get('faq_page')) {
                    $faq_page = $this->input->get('faq_page');
                    $offset =  ($faq_page - 1) * $this->page_line_max;
                }

                $consultation_list_ar = $this->Mbbs->search_consultation_list($this->page_line_max, $offset, $big_category_id);

                /* パンくず */
                $this->viewData['breadcrumb_list'] = array(
                                                        array(
                                                            'url' => base_url(),
                                                            'text' => 'トップページ',
                                                        ),
                                                        array(
                                                            'url' => base_url().'cate'.$big_category_id,
                                                            'text' => $big_category_name,
                                                        ),
                                                    );


                $this->get_meta_tags($big_category_id, 1); // call for meta value
                $this->viewData['consultation_list_ar'] = $consultation_list_ar;
                $this->template = 'search/category.html';
            }
        } else {
            $big_category_info_ar = $this->Madmin->get_big_category($big_category_id);
            $this->viewData['big_category_info'] = $big_category_info_ar[0];
            $big_category_name = $big_category_info_ar[0]['name'];

            $category_info_ar = $this->Madmin->get_category($category_id);
            if (empty($big_category_info_ar) || empty($category_info_ar)) {
                $this->template = '404.html';
            } else {
                $this->viewData['category_info'] = $category_info_ar[0];
                $category_name = $category_info_ar[0]['name'];

                $sidebar_param = array(
                    'row' => 'bt.cate_id',
                    'value' => $category_id 
                );

                $category_info_other_ar = $this->Madmin->get_categorys($big_category_id);
                foreach ($category_info_other_ar as $key => $val) {
                    $category_info_other_ar[$key]['current'] = ($val['id'] == $category_id)? 'current':'';
                }
                $this->viewData['category_info_ar'] = $category_info_other_ar;

                $list_all_num = $this->Mbbs->search_consultation_list_num($big_category_id, $category_id);
                // pagination
                $this->set_pagination('category', $list_all_num, $big_category_id, $category_id);

                $offset = $faq_page = null;
                if ($this->input->get('faq_page')) {
                    $faq_page = $this->input->get('faq_page');
                    $offset =  ($faq_page - 1) * $this->page_line_max;
                }

                $consultation_list_ar = $this->Mbbs->search_consultation_list($this->page_line_max, $offset, $big_category_id, $category_id);
                
                $this->viewData['consultation_list_ar'] = $consultation_list_ar;

                /* パンくず */
                $this->viewData['breadcrumb_list'] = array(
                                                        array(
                                                            'url' => base_url(),
                                                            'text' => 'トップページ',
                                                        ),
                                                        array(
                                                            'url' => base_url().'cate'.$big_category_id,
                                                            'text' => $big_category_name,
                                                        ),
                                                        array(
                                                            'url' => base_url().'cate'.$big_category_id.'/'.$category_id,
                                                            'text' => $category_name,
                                                        ),
                                                    );
                $this->get_meta_tags($category_id, 2); // call for meta value

                $this->template = 'search/category_detail.html';
            }
        }
        $this->sidebar($this->page_line_max, $sidebar_param);

        if ($this->agent->is_mobile()) {
            $this->twig->template_dir_set(TEMPLATEPATH_SP);
            $this->twig->display($this->template, $this->viewData);
        } else {
            $this->twig->template_dir_set(TEMPLATEPATH_PC);
            $this->twig->display($this->template, $this->viewData);
//            $this->twig->display('bb/consultation_list.html', $this->viewData);
        }

    }

    public function search_consultation() {
        echo "相談検索";

    }

    public function set_pagination($target = null, $list_all_num, $big_category_id = null, $category_id = null) {
        $this->load->library('pagination');

        if ($target == 'category') {
            if ($category_id != null) {
                $category_id = '/'.$category_id;
            }            
            $config['suffix'] = '#consultation_list';
            $config['first_url'] = base_url().'cate'.$big_category_id.$category_id.'#consultation_list';
            $config['base_url'] = base_url().'cate'.$big_category_id.$category_id;
        } else if ($target == 'search_keyword') {
            $config['base_url'] = base_url().'search';
        }

        $config['total_rows'] = $list_all_num;
        $config['per_page'] = $this->page_line_max;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'faq_page';
        $config['num_links'] = 2;

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li>';
        $config['prev_link'] = '前へ';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_link'] = '次へ';
        $config['next_tag_close'] = '</li>';

        $config['use_page_numbers'] = TRUE;
        $config['cur_tag_open'] = '<li><a class="pager_style current" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['attributes'] = array('class' => 'pager_style');
        $config['attributes']['rel'] = FALSE;

        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;

        $this->pagination->initialize($config);

        $this->viewData['pagination'] = $this->pagination->create_links();
    }

}
