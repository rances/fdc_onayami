<?php
class MY_Controller extends CI_Controller {
	public $viewData = array();
	public $user;
    public $meta_array = array(); // array of list meta tags
    public $big_category_name;
    public $category_name;
    public $big_category_id;
    public $category_id;
    public $contents_id;
    function __construct()
    {
        parent::__construct();
        
        $this->load->library('twig');
        $this->load->model("Madmin");
      	$this->viewData['main_url'] = MAIN_URL;
        $this->twig->template_dir_set(($this->agent->is_mobile()) ? TEMPLATEPATH_SP : TEMPLATEPATH_PC);

        $this->user = UserControl::getuser();
        if ($this->user) {
            $this->viewData['user_login_flag'] = true;

            $this->user['user_can_point'] = (!empty($this->user['user_from_site']) && in_array($this->user['user_from_site'], array(1,2))) ? true : false;
        	$this->viewData['user_can_point'] = $this->user['user_can_point'];
        } else {
            $this->viewData['user_login_flag'] = false;
        }

        $this->init_meta(); 
    }

    /**
     * Initialize data for meta values
     */
    public function init_meta() {
        $this->load->model("Mbbs");
        $temp = '';
        $this->meta_array = array();
        $row = $this->Mbbs->getCategoryMeta();
        foreach ($row as $key => $value) {
            if ($temp != $value['id']) {
                $temp = $value['id'];
                $this->meta_array[1][$temp] = array(
                      'title' => $value['b_title'],
                      'description' => $value['b_desc']
                    );
                $this->meta_array[2][$value['sub_id']] = array(
                      'title' => $value['sub_title'],
                      'description' => $value['sub_description']
                    );
            } else {
                $this->meta_array[2][$value['sub_id']] = array(
                      'title' => $value['sub_title'],
                      'description' => $value['sub_description']
                    );
            }
        }
    }
    
    /**
     * Select meta value according to its id
     * @param string $id : ID either Main Category or Sub Category
     * @param string $type : Type if 1 = Main Category , 2 = Sub Category
     */
    public function get_meta_tags($id = null, $type = null) {
        $metag_list = $this->meta_array[$type][$id];
        $this->viewData['title'] = $metag_list['title'];
        $meta_desc = preg_replace("/[\n\r]/","",$metag_list['description']);
        $meta_desc = str_replace(' ', '', $meta_desc);
        $this->viewData['description'] = $meta_desc;
    }

    public function bread_crumb($id = null , $arr = array(), $name = 'みんなの回答') {
        if (empty($arr)) {
            $bread = array(
                    array(
                        'url' => base_url(),
                        'text' => 'トップページ',
                    ),
                    array(
                        'url' => base_url().'cate'.$this->big_category_id,
                        'text' => $this->big_category_name,
                    ),
                    array(
                        'url' => base_url().'cate'.$this->big_category_id.'/'.$this->category_id,
                        'text' => $this->category_name,
                    ),
                    array(
                        'url' => base_url().'cate'.$this->big_category_id.'/'.$this->category_id.'/'.$this->contents_id,
                        'text' => $name,
                    ),
                );
        } else {
            $bread = $arr;
        }
        $this->sub_category($this->big_category_id);
        return $bread;
    }

    public function sidebar($limit = 10, $param = array()) {
        $consultation_list_all_ar = $this->Mbbs->get_consultation_list_all($limit, $param, null, 'sidebar');
        $this->viewData['consultation_list_all_ar'] = $consultation_list_all_ar;
    }

    public function sub_category($big_id = null) {
        $category_info_other_ar = $this->Madmin->get_categorys($big_id);
        foreach ($category_info_other_ar as $key => $val) {
            $category_info_other_ar[$key]['current'] = ($val['id'] == $this->category_id)? 'current':'';
        }
        $this->viewData['category_info_ar'] = $category_info_other_ar;
    }

}