<?php
    class Mcommentrank extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }
		
        /**
         * Update bbs_messages_rank table based on new bbs_messages data
         * 
         */
        function updateRanking($id = null) {
            $sql = "INSERT INTO bbs_messages_rank(id,like_count,rank,create_date)
                SELECT 
                    c.id,
                    c.evaluate,
                    ROUND(((@rank - rank) / @rank) * 100, 2) AS rank,
                    current_timestamp 
                FROM
                    (SELECT 
                        *,
                            @prev:=@curr,
                            @curr:=abt.evaluate,
                            @rank:=IF(@prev = @curr, @rank, @rank + 1) AS rank
                    FROM
                        (SELECT 
                        id, evaluate
                    FROM
                        bbs_messages
                    WHERE
                        display_flag = 1) AS abt, (SELECT @curr:=null, @prev:=null, @rank:=0) AS b
                    ORDER BY evaluate DESC) AS c

                ON DUPLICATE KEY UPDATE
                  like_count     = VALUES(like_count),
                  rank = VALUES(rank),
                  update_date = current_timestamp
                ";
            $this->db->query($sql);
            return $this->db->affected_rows();
        }
		

    }
